package mk.iwec.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import mk.iwec.model.Student;

@Service
public class StudentService {
	private Map<Integer, Student> repository = Arrays.asList(
			new Student[] {
					new Student (1,"Pero","Perov"),
					new Student (2,"Tosho","Toshov"),
					new Student (3,"Mile","Milev"),
			}).stream().collect(Collectors.toConcurrentMap(s -> s.getId(), Function.identity()));
	
	private AtomicInteger sequence = new AtomicInteger(3);
	
	public List<Student> readAll() {
		return repository.values().stream().collect(Collectors.toList());
	}
	
	public Student read(Integer id) {
		return repository.get(id);
		}
	
	public Student create (Student student) {
		int key = sequence.incrementAndGet();
		student.setId(key);
		repository.put(key, student);
		return student;
	}
	public Student update(Integer id, Student student) {
		student.setId(id);
		Student oldStudent = repository.replace(id, student);
		return oldStudent == null ? null : student;
	}
	public void delete (Integer id) {
		repository.remove(id);
		}
	}
