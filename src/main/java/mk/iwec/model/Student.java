package mk.iwec.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Student {
	public int Id;
	public String firstName;
	public String lastName;
}
